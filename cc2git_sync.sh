
#!/bin/bash

### SCRIPT: For complete versions in selected versions with each check-in history
### This script is used to pick all versions of each file in selected branch during initial dump & also during later synchronization
### All these versions will be committed seperately with its own check-in history thus having multiple commit revisions

### SCRIPT: One commit with accumulated check-in commit with all previous check-in comments
### There is another script available only for a single commit with all accumulated versions history of that selected branch while initial dump
### Later subsequent synchronization will pick up the updated versions and update that many commits respectively.

### Setting environment variables
export PATH=$PATH:/usr/bin:/usr/sbin:/usr/local/bin:/opt/rational/clearcase/bin
ViewTag=$1
VOBTag=$2
Git_RepoPath=$3
GitBranch=$4

CurrDir=$PWD
LabelInt="^APS_LABEL|_VI_CMREPO$"               ### Customizable
umask 000
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
logDir="$SCRIPTPATH"/logs/`echo $VOBTag|sed "s/\//_/g"`_`date +%d-%m-%Y_%H%M`

### Validating total arguments
if [[ $# -ne 4 ]]; then
   echo; Count=50; while [[ $Count -ne 0 ]]; do echo -e "#\c"; Count=`expr $Count - 1`; done; echo
   echo -e "<=== Three arguments are missing ===>\nUsage  : $0 <ViewTag> <VOBTag> <GitRepoPath> <GitBranch>\nExample: $0 cmadmblr_ims_10.1 /vobs/ims_wintest /imsgit/git_workdir/ims_wintest"
   Count=50; while [[ $Count -ne 0 ]]; do echo -e "#\c"; Count=`expr $Count - 1`; done; echo -e "\n"
   exit
fi

### Validate the view Tag & its access
cleartool lsview $ViewTag >/dev/null 2>&1
if [ $? -ne 0 ]; then
   echo -e "\nView tag '$ViewTag' does not exist\n"; exit
else
   if [[ ! -d "/view/$ViewTag" ]]; then
      cleartool startview $ViewTag >/dev/null 2>&1
      if [[ $? -ne 0 ]]; then
         echo -e "\nError starting view '$ViewTag'\n"
         cleartool startview $ViewTag; exit
      fi
   fi
fi

### Validate the VOB tag and its access
cleartool lsvob $VOBTag >/dev/null 2>&1
if [ $? -ne 0 ]; then
   echo -e "\nVOB tag '$VOBTag' does not exist\n"; exit
else
   if [[ ! `cleartool lsvob $VOBTag |grep "^\* "` ]]; then
      cleartool mount $VOBTag >/dev/null 2>&1
      if [[ $? -ne 0 ]]; then
         echo -e "\nError mounting VOB '$VOBTag'\n"
         cleartool mount $VOBTag
      fi
   fi
fi

### Checking the local working copy. It should be a cloned one
if [[ ! -d $Git_RepoPath/.git ]]; then echo -e "\nError - Mentioned Git repo path is not a cloned one\n"; exit; fi

if [[ ! -d $logDir ]]; then mkdir -p $logDir 2>/dev/null; fi
if [[ $? -ne 0 ]]; then echo -e "\nUnable to create log directory '$logDir'...\n"; exit; fi

### Checking the branch existance
cd $Git_RepoPath
if [[ `git branch | grep -w "$GitBranch"` ]]; then
   git checkout $GitBranch 1>$logDir/currBranch.txt 2>&1
else
   echo -e "\nNo Branch exists with name '$GitBranch' in Git repo path '$Git_RepoPath' \n"
   exit
fi

### Doing rsync from a VOB's path to Git repo using a ClearCase dynamic view
rsyncSuccess=$logDir/rsync_success.txt
rsyncFailure=$logDir/rsync_failure.err
echo -e "Doing rsync from '$VOBTag' VOB using '$ViewTag' view towards \"$Git_RepoPath\" Git repo"
#cleartool setview -exec "rsync -avz --delete --exclude-from=rsync_Exclude/ims_admin.txt $VOBTag/* $Git_RepoPath 1>>$rsyncSuccess 2>>$rsyncFailure" $ViewTag
cleartool setview -exec "rsync -avz --delete --exclude=".git/" --exclude="lost+found" $VOBTag/* $Git_RepoPath 1>>$rsyncSuccess 2>>$rsyncFailure" $ViewTag

listOfFiles=$logDir/listOfFiles.txt
listOfFilesNew=$logDir/listOfFiles-new.txt
listOfFilesUpd=$logDir/listOfFiles-upd.txt
listOfFilesDel=$logDir/listOfFiles-del.txt
echo -e "Collecting modified files from Git working copy '$Git_RepoPath'"
git status --porcelain --untracked-files > $listOfFiles
echo -e "Filtering new/un-tracked files"
egrep "^\?\? |^A[ M] " $listOfFiles > $listOfFilesNew
echo -e "Filtering only modified files"
egrep "^ M |^MM " $listOfFiles > $listOfFilesUpd
egrep "^D |^ D" $listOfFiles > $listOfFilesDel

echo -e "Checking new files & adding ClearCase view and version extended path"
CC_FileVer_new=$logDir/CC_FileVer-new.txt
while read line
do
   Element=$(echo $line|cut -d' ' -f2-|tr -d '"')
   cleartool ls -s -vob_only "/view/$ViewTag$VOBTag/$Element" 2>/dev/null
done < $listOfFilesNew > $CC_FileVer_new

CC_FileVer_upd=$logDir/CC_FileVer-upd.txt
while read line
do
   Element=$(echo $line|cut -d' ' -f2-|tr -d '"')
   cleartool ls -s -vob_only "/view/$ViewTag$VOBTag/$Element" 2>/dev/null
done < $listOfFilesUpd > $CC_FileVer_upd

echo -e "Traversing all previous version on current branch till '0' for all new files"
git_cmd_success=$logDir/git_cmd_new_success-output.txt
git_cmd_failure=$logDir/git_cmd_new_failure-output.err
cat /dev/null > $git_cmd_success; cat /dev/null > $git_cmd_failure
TagDel=`echo /view/$ViewTag$VOBTag|awk -F'/' '{print NF}'`
TagDel=`expr $TagDel + 1`
while read Element; do
   if [[ `echo "$Element"|grep "@@\/main"` ]]; then
      TotField=$(echo $Element |awk -F'/' '{print NF}')
      CurrRevNo=$(echo $Element|awk -F'/' '{print $NF}')
      BrExtPath=$(echo $Element|cut -d'/' -f1-`expr $TotField - 1`)
      AbsPath=$(echo $Element|awk -F'@@/' '{print $1}'|cut -d'/' -f$TagDel-)
      for ((Version=0;Version<=$CurrRevNo;Version++)); do
         #cleartool setview -exec "cp -f '$BrExtPath'/$Version '$AbsPath'" $ViewTag
         if [[ -f "$BrExtPath/$Version" || -d "$BrExtPath/$Version" ]]; then
            cp -f "$BrExtPath/$Version" "$AbsPath"
            #cleartool get -to "$AbsPath" "$BrExtPath"/$Version
            CI_details=$(cleartool desc -fmt 'CC_Ver: %Sn |Created: %Vd|Owner: %u|Labels: %l|Comment: %c' "$BrExtPath"/$Version)
            ReqLabels=`echo $CI_details|cut -d'|' -f4|tr -d ":(),"| tr -s " " "\n"|egrep "$LabelInt"`
            CI_comment=$(echo $CI_details|awk -v Labels="`echo $ReqLabels|sed "s/ /, /g"`" -F'|' '{print $1"|"$2"|"$3"|"$5"|Labels: "Labels}')
            Date=$(echo $CI_comment|awk -F'|' '{print $2}'|cut -d':' -f2-)
            Author=$(echo $CI_comment|awk -F'|' '{print $3}'|cut -d':' -f2)" <noemail@noemail.com>"
            git add "$AbsPath" 1>>$git_cmd_success 2>>$git_cmd_failure
            git commit -m "$CI_comment" --date="$Date" --author="$Author" "$AbsPath" 1>>$git_cmd_success 2>>$git_cmd_failure
         fi
      done
   else
      AbsPath=$(echo $Element|cut -d'/' -f$TagDel-)
      if [ -L "$AbsPath" ]; then
         CI_comment=$(cleartool desc -fmt 'CC_Ver: SymbolicLink |Created: %Vd|Owner: %u|Comment: %c|Labels: %l' "$Element")
         Date=$(echo $CI_comment|awk -F'|' '{print $2}'|cut -d':' -f2-)
         Author=$(echo $CI_comment|awk -F'|' '{print $3}'|cut -d':' -f2)" <noemail@noemail.com>"
         git add "$AbsPath" 1>>$git_cmd_success 2>>$git_cmd_failure
         git commit -m "$CI_comment" --date="$Date" --author="$Author" "$AbsPath" 1>>$git_cmd_success 2>>$git_cmd_failure
      fi
   fi
done < $CC_FileVer_new

echo -e "Traversing till the previous version that is already in Git repo"
ChangeSetVersions=$logDir/ChangeSetVersions.txt
cat /dev/null > $ChangeSetVersions
while read Element; do
   Version_details=$logDir/File_VersionDetails.txt
   TotField=$(echo $Element |awk -F'/' '{print NF}')
   CurrRevNo=$(echo $Element|awk -F'/' '{print $NF}')
   BrExtPath=$(echo $Element|cut -d'/' -f1-`expr $TotField - 1`)
   Branch_Int=$(echo $BrExtPath|awk -F'/' '{print $NF}')
   ElementOnly=$(echo $Element|cut -d'@' -f1)
   AbsPath=$(echo $Element|cut -d'@' -f1|cut -d'/' -f$TagDel-)
   LastCommitComment=$(git log --oneline "$AbsPath"| grep " CC_Ver: "|head -1|cut -d' ' -f2-)
   LastCommitVersion=$(echo $LastCommitComment|cut -d' ' -f2)
   #Branch_Int=$(echo $LastCommitVersion|awk -F'/' '{S=NF-1; print $S}')
   Cmd=$(echo "cleartool find $ElementOnly -version 'brtype($Branch_Int)' -print | grep -v 'CHECKEDOUT$'")
   eval $Cmd > $Version_details
   From_line=$(expr `grep -n "@@$LastCommitVersion$" $Version_details|cut -d':' -f1` + 1)
   Tot_line=`wc -l < $Version_details`
   if [[ $Tot_line -ge $From_line ]]; then
      sed -n "$From_line,$Tot_line p" < $Version_details >> $ChangeSetVersions
   fi
done < $CC_FileVer_upd

git_cmd_success=$logDir/git_cmd_upd_success-output.txt
git_cmd_failure=$logDir/git_cmd_upd_failure-output.err
cat /dev/null > $git_cmd_success; cat /dev/null > $git_cmd_failure
while read chgdFileVers; do
   TotField=$(echo $chgdFileVers |awk -F'/' '{print NF}')
   AbsPath=$(echo $chgdFileVers|cut -d'@' -f1|cut -d'/' -f$TagDel-)
   cp -f "$chgdFileVers" "$AbsPath"
   CI_details=$(cleartool desc -fmt 'CC_Ver: %Sn |Created: %Vd|Owner: %u|Labels: %l|Comment: %c' "$chgdFileVers")
   ReqLabels=`echo $CI_details|cut -d'|' -f4|tr -d ":(),"| tr -s " " "\n"|egrep "$LabelInt"`
   CI_comment=$(echo $CI_details|awk -v Labels="`echo $ReqLabels|sed "s/ /, /g"`" -F'|' '{print $1"|"$2"|"$3"|"$5"|Labels: "Labels}')
   Date=$(echo $CI_comment|awk -F'|' '{print $2}'|cut -d':' -f2-)
   Author=$(echo $CI_comment|awk -F'|' '{print $3}'|cut -d':' -f2)" <noemail@noemail.com>"
   git add "$AbsPath" 1>>$git_cmd_success 2>>$git_cmd_failure
   git commit -m "$CI_comment" --date="$Date" --author="$Author" "$AbsPath" 1>>$git_cmd_success 2>>$git_cmd_failure
done < $ChangeSetVersions

echo -e  "Handling 'deletion' for the marked files"
git_cmd_success=$logDir/git_cmd_del_success-output.txt
git_cmd_failure=$logDir/git_cmd_del_failure-output.err
cat /dev/null > $git_cmd_success; cat /dev/null > $git_cmd_failure
while read delFile; do
   Element=$(echo $delFile|cut -d' ' -f2-|tr -d '"')
   git rm "$Element" 1>>$git_cmd_success 2>>$git_cmd_failure
   git commit -m "Deleting as marked for Deletion" "$Element" 1>>$git_cmd_success 2>>$git_cmd_failure
done < $listOfFilesDel

cd $CurrDir